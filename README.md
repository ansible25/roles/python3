Python3
=========

Role that installs Python3 and Pip

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Python3 on targeted machine:

    - hosts: servers
      roles:
         - python3
License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
